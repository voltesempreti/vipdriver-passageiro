package com.vipdriver.user.base;

import androidx.fragment.app.FragmentActivity;

public interface MvpView {

    FragmentActivity baseActivity();

    void showLoading();

    void hideLoading() throws Exception;

    void onSuccessLogout(Object object);

    void onError(Throwable throwable);
}
