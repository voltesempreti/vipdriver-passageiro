package com.vipdriver.user.data.network.model;

public class WalkThrough {

    public String title, description;
    public int raw;

    public WalkThrough(String title, String description, int raw) {
        this.title = title;
        this.description = description;
        this.raw = raw;
    }

}
