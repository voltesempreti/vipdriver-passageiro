package com.vipdriver.user.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.GeolocationPermissions;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.vipdriver.user.R;
import com.vipdriver.user.ui.activity.main.MainActivity;

public class DeliveryActivity extends AppCompatActivity {

    public AlertDialog.Builder dialog;
    public WebView webView;
    public ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressBar = findViewById(R.id.progress);
        webView = findViewById(R.id.web_view_delivery);

        progressBar.setVisibility(View.INVISIBLE);

        webView.setWebViewClient(new MyBrowser());
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.setWebViewClient(new WebViewClient());
        webView.setWebChromeClient(new GeoWebChromeClient(this));
        webView.getSettings().setGeolocationDatabasePath(getApplicationContext().getFilesDir().getPath());

        dialog = new AlertDialog.Builder(this);
        abrirPagina();

        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progressBar.setVisibility(View.VISIBLE); // mostra a progress
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressBar.setVisibility(View.INVISIBLE); // esconde a progress
            }
        });
    }

    private void abrirPagina() {
        try {
            WebSettings ws = webView.getSettings();

            if (true) {
                progressBar.setVisibility(View.INVISIBLE);
                String url = "https://www.vaija.club/";

                ws.setAllowFileAccess(true);
                ws.setGeolocationEnabled(true);
                ws.setCacheMode(WebSettings.LOAD_DEFAULT);
                ws.setJavaScriptEnabled(true);
                ws.setAppCacheMaxSize(5 * 1024 * 1024); //5mb
                ws.setSupportZoom(false);
                ws.setAppCacheEnabled(true);
                ws.setLoadsImagesAutomatically(true);
                ws.setAppCacheEnabled(true);
                ws.setDatabaseEnabled(true);
                ws.setDomStorageEnabled(true);
                ws.setJavaScriptCanOpenWindowsAutomatically(true);

                webView.loadUrl(url);

            } else {
                progressBar.setVisibility(View.VISIBLE);
                ws.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
                Toast.makeText(this, "No momento você está sem conexão com a internet.",
                        Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivityForResult(myIntent, 0);
        finish();
        return true;
    }

}



class MyBrowser extends WebViewClient {
    public  boolean overrideURLLoading (WebView view, String url){
        view.loadUrl(url);
        return true;
    }
}

/**
 * A subclasse WebChromeClient lida com chamadas relacionadas à UI
 */
class GeoWebChromeClient extends WebChromeClient {

    public AlertDialog.Builder dialog;
    private Context context;

    public GeoWebChromeClient(Context context){
        this.context = context;
    }

    @Override
    public void onGeolocationPermissionsShowPrompt(final String origin,
                                                   final GeolocationPermissions.Callback callback) {

        dialog = new AlertDialog.Builder(context);

//dialog perguntando se o usuário permite ou não pegar sua localização
        dialog.setTitle("Acessar sua localização");
        dialog.setMessage("O aplicativo quer acessar sua localização. Você" +
                " permite?");
        dialog.setCancelable(false);
        dialog.setPositiveButton("Permitir", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                callback.invoke(origin, true, true);
                Log.i("localizacao", origin);
            }
        });
        dialog.setNegativeButton("Negar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                callback.invoke(origin, false, true);
                Log.i("localizacao", origin);
            }
        });
        dialog.create();
        dialog.show();

    }
}