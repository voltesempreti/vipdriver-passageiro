package com.vipdriver.user.ui.activity.invite_friend;

import com.vipdriver.user.base.MvpView;
import com.vipdriver.user.data.network.model.User;

public interface InviteFriendIView extends MvpView {

    void onSuccess(User user);

    void onError(Throwable e);

}
