package com.vipdriver.user.ui.activity.passbook;

import com.vipdriver.user.base.MvpPresenter;

public interface WalletHistoryIPresenter<V extends WalletHistoryIView> extends MvpPresenter<V> {
    void wallet();
}
