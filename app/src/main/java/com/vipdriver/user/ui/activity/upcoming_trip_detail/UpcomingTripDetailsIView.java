package com.vipdriver.user.ui.activity.upcoming_trip_detail;

import com.vipdriver.user.base.MvpView;
import com.vipdriver.user.data.network.model.Datum;

import java.util.List;

public interface UpcomingTripDetailsIView extends MvpView {

    void onSuccess(List<Datum> upcomingTripDetails);

    void onError(Throwable e);
}
