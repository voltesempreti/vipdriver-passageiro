package com.vipdriver.user.ui.fragment.book_ride;

import com.vipdriver.user.base.MvpView;
import com.vipdriver.user.data.network.model.PromoResponse;


public interface BookRideIView extends MvpView {
    void onSuccess(Object object);

    void onError(Throwable e);

    void onSuccessCoupon(PromoResponse promoResponse);
}
