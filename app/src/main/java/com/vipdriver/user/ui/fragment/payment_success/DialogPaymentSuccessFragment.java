package com.vipdriver.user.ui.fragment.payment_success;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.vipdriver.user.BuildConfig;
import com.vipdriver.user.R;
import com.vipdriver.user.data.SharedHelper;
import com.vipdriver.user.data.network.model.User;
import com.vipdriver.user.ui.activity.main.MainActivity;

import androidx.fragment.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static com.vipdriver.user.data.SharedHelper.key.PROFILE_IMG;

public class DialogPaymentSuccessFragment extends DialogFragment {

    private View root_view;
    private TextView date_payment;
    private TextView hour_payment;
    private TextView nameProfilePayment;
    private TextView email_profile_payment;
    private TextView amountTransferred;
    private CircularImageView picture_profile_payment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root_view = inflater.inflate(R.layout.fragment_dialog_payment_success, container, false);
        setCancelable(false);

        String amount = this.getArguments().getString("amountTransferred");

        date_payment = root_view.findViewById(R.id.date_payment);
        hour_payment = root_view.findViewById(R.id.hour_payment);
        nameProfilePayment = root_view.findViewById(R.id.nameProfilePayment);
        email_profile_payment = root_view.findViewById(R.id.email_profile_payment);
        amountTransferred = root_view.findViewById(R.id.amountTransferred);
        picture_profile_payment = root_view.findViewById(R.id.picture_profile_payment);

        amountTransferred.setText(amount);

        setData();

        ((FloatingActionButton) root_view.findViewById(R.id.fab)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });

        return root_view;
    }

    public void setData(){

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat currentTimeFormat = new SimpleDateFormat("HH:mm");

        Date data = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        Date data_atual = cal.getTime();
        String data_completa = dateFormat.format(data_atual);
        String currentTime = currentTimeFormat.format(data_atual);

        User user = new Gson().fromJson(SharedHelper.getKey(getContext(), "userInfo"), User.class);
        if (user != null) {
            nameProfilePayment.setText(String.format("%s %s", user.getFirstName(), user.getLastName()));
            email_profile_payment.setText(user.getEmail());
        }

        SharedHelper.putKey(getContext(), PROFILE_IMG, user.getPicture());
        Glide.with(getActivity())
                .load(BuildConfig.BASE_IMAGE_URL + user.getPicture())
                .apply(RequestOptions.placeholderOf(R.drawable.ic_user_placeholder)
                        .dontAnimate()
                        .error(R.drawable.ic_user_placeholder))
                .into(picture_profile_payment);


        date_payment.setText(data_completa);
        hour_payment.setText(currentTime);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}