package com.vipdriver.user.ui.activity.help;

import com.vipdriver.user.base.MvpView;
import com.vipdriver.user.data.network.model.Help;

public interface HelpIView extends MvpView {

    void onSuccess(Help help);

    void onError(Throwable e);
}
