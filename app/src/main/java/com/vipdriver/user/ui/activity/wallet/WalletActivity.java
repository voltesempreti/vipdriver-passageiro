package com.vipdriver.user.ui.activity.wallet;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.vipdriver.user.R;
import com.vipdriver.user.base.BaseActivity;
import com.vipdriver.user.common.Constants;
import com.vipdriver.user.common.mask.MoneyTextWatcher;
import com.vipdriver.user.data.SharedHelper;
import com.vipdriver.user.data.network.model.AddWallet;
import com.vipdriver.user.data.network.model.WalletResponse;
import com.vipdriver.user.ui.activity.payment.PaymentActivity;
import com.vipdriver.user.ui.adapter.WalletAdapter;
import com.vipdriver.user.ui.fragment.payment_success.DialogPaymentSuccessFragment;

import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.vipdriver.user.MvpApplication.isCard;
import static com.vipdriver.user.ui.activity.payment.PaymentActivity.PICK_PAYMENT_METHOD;

public class WalletActivity extends BaseActivity implements WalletIView {

    @BindView(R.id.wallet_balance)
    TextView walletBalance;
    @BindView(R.id.tvNoWalletData)
    TextView tvNoWalletData;
    @BindView(R.id.amount)
    EditText amount;
    @BindView(R.id.add_amount)
    Button addAmount;
    @BindView(R.id.rvWalletData)
    RecyclerView rvWalletData;


    String regexNumber = "^(\\d{0,9}\\.\\d{1,4}|\\d{1,9})$";
    private WalletPresenter<WalletActivity> presenter = new WalletPresenter<>();
    private double walletAmt;

    private ProgressBar progress_bar;

    @Override
    public int getLayoutId() {
        return R.layout.activity_wallet;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void initView() {

        ButterKnife.bind(this);
        presenter.attachView(this);
        // Activity title will be updated after the locale has changed in Runtime
        setTitle(getString(R.string.wallet));
        showLoading();

        Locale mLocale = new Locale("pt", "BR");
        amount.addTextChangedListener(new MoneyTextWatcher(amount, mLocale));

        progress_bar = findViewById(R.id.progress_bar);
        amount.setTag(SharedHelper.getKey(this, "currency"));
//        walletBalance.setText(getNumberFormat().format(Double.parseDouble(SharedHelper.getKey(this, "walletBalance", "0"))));

        if (!isCard) {
            addAmount.setVisibility(View.GONE);
        }

        rvWalletData.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvWalletData.setItemAnimator(new DefaultItemAnimator());
        rvWalletData.setHasFixedSize(true);

        presenter.wallet();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @OnClick({R.id.add_amount})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.add_amount:
                if (validateAmount()) {
                    Intent intent = new Intent(baseActivity(), PaymentActivity.class);
                    intent.putExtra("hideCash", true);
                    startActivityForResult(intent, PICK_PAYMENT_METHOD);
                }
                break;
        }
    }

    public boolean validateAmount(){
        if(amount.getText().toString().equals("")){
            Toast.makeText(this, "Preencha o valor do saldo antes de adicionar!", Toast.LENGTH_SHORT).show();
            return false;
        }


        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_PAYMENT_METHOD && resultCode == Activity.RESULT_OK && data != null)
            switch (data.getStringExtra("payment_mode")) {
                case Constants.PaymentMode.CARD:
                    String stringValue = amount.getText().toString().replaceAll("R", "").replaceAll("\\$", "").replaceAll("\\s+", "").replaceAll(",", ".");
                    double value = Double.parseDouble(stringValue);
                    HashMap<String, Object> map = new HashMap<>();
                    String cardId = data.getStringExtra("card_id");
                    map.put("amount", value);
                    map.put("payment_mode", "CARD");
                    map.put("card_id", cardId);
                    map.put("user_type", "user");
                    showLoading();
                    presenter.addMoney(map);
                    break;

                }
    }

    @Override
    public void onSuccess(AddWallet wallet) {
        try {
            hideLoading();
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        showDialogPaymentSuccess();
        progress_bar.setVisibility(View.VISIBLE);

        SharedHelper.putKey(this, "walletBalance", String.valueOf(wallet.getBalance()));
        //walletBalance.setText(getNumberFormat().format(Double.parseDouble(SharedHelper.getKey(this, "walletBalance", "0"))));
    }

    private void showDialogPaymentSuccess() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        DialogPaymentSuccessFragment newFragment = new DialogPaymentSuccessFragment();
        Bundle bundle = new Bundle();
        bundle.putString("amountTransferred", amount.getText().toString());
        newFragment.setArguments(bundle);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, newFragment).addToBackStack(null).commit();
    }

    @Override
    public void onError(Throwable e) {
        handleError(e);
    }

    @Override
    public void onSuccess(WalletResponse response) {
        try {
            hideLoading();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        if (!response.getWallets().isEmpty()) {
            walletAmt = response.getWalletBalance();
            if(walletAmt < 0){
                walletBalance.setTextColor(ContextCompat.getColor(this, R.color.red));
            } else if(walletAmt > 0){
                walletBalance.setTextColor(ContextCompat.getColor(this, R.color.green));
            }
            walletBalance.setText("R$" + String.format("%.2f", walletAmt));
            if (response.getWallets() != null && response.getWallets().size() > 0) {
                rvWalletData.setAdapter(new WalletAdapter(response.getWallets()));
                //llWalletHistory.setVisibility(View.VISIBLE);
                //tvWalletPlaceholder.setVisibility(View.GONE);
            } else {
                //llWalletHistory.setVisibility(View.GONE);
                //tvWalletPlaceholder.setVisibility(View.VISIBLE);
            }
        } else tvNoWalletData.setVisibility(View.VISIBLE);
    }

}
