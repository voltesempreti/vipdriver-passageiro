package com.vipdriver.user.ui.activity.notification_manager;

import com.vipdriver.user.base.MvpView;
import com.vipdriver.user.data.network.model.NotificationManager;

import java.util.List;

public interface NotificationManagerIView extends MvpView {

    void onSuccess(List<NotificationManager> notificationManager);

    void onError(Throwable e);

}