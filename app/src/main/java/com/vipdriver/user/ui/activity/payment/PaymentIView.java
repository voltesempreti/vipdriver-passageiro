package com.vipdriver.user.ui.activity.payment;

import com.vipdriver.user.base.MvpView;
import com.vipdriver.user.data.network.model.Card;

import java.util.List;

public interface PaymentIView extends MvpView {

    void onSuccess(Object card);

    void onSuccess(List<Card> cards);

    void onAddCardSuccess(Object cards);

    void onError(Throwable e);

}
