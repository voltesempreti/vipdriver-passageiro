package com.vipdriver.user.ui.fragment.invoice;

import com.vipdriver.user.base.MvpView;
import com.vipdriver.user.data.network.model.Message;

public interface InvoiceIView extends MvpView {

    void onSuccess(Message message);

    void onSuccess(Object o);

    void onSuccessVirtualChange(Object o);

    void onSuccessPayment(Object o);

    void onError(Throwable e);

    void onErrorVirtualChange(Throwable e);

}
