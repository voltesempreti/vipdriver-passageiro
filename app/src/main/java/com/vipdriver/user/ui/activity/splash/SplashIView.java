package com.vipdriver.user.ui.activity.splash;

import com.vipdriver.user.base.MvpView;
import com.vipdriver.user.data.network.model.CheckVersion;
import com.vipdriver.user.data.network.model.Service;
import com.vipdriver.user.data.network.model.User;

import java.util.List;

public interface SplashIView extends MvpView {

    void onSuccess(List<Service> serviceList);

    void onSuccess(User user);

    void onError(Throwable e);

    void onSuccess(CheckVersion checkVersion);
}
