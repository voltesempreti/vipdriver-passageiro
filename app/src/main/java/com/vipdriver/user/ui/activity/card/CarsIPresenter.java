package com.vipdriver.user.ui.activity.card;

import com.vipdriver.user.base.MvpPresenter;


public interface CarsIPresenter<V extends CardsIView> extends MvpPresenter<V> {
    void card();
}
