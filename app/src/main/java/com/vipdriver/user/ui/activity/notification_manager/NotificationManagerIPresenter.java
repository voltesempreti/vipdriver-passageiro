package com.vipdriver.user.ui.activity.notification_manager;

import com.vipdriver.user.base.MvpPresenter;

public interface NotificationManagerIPresenter<V extends NotificationManagerIView> extends MvpPresenter<V> {
    void getNotificationManager();
}
