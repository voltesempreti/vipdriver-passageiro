package com.vipdriver.user.ui.fragment.searching;

import com.vipdriver.user.base.MvpView;

public interface SearchingIView extends MvpView {
    void onSuccess(Object object);

    void onError(Throwable e);
}
