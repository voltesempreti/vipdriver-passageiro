package com.vipdriver.user.ui.activity.coupon;

import com.vipdriver.user.base.MvpPresenter;

public interface CouponIPresenter<V extends CouponIView> extends MvpPresenter<V> {
    void coupon();
}
