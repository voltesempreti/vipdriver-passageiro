package com.vipdriver.user.ui.fragment.dispute;

public interface DisputeCallBack {
    void onDisputeCreated();
}
