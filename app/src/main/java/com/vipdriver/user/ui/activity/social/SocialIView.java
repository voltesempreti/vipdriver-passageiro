package com.vipdriver.user.ui.activity.social;

import com.vipdriver.user.base.MvpView;
import com.vipdriver.user.data.network.model.Token;

/**
 * Created by santhosh@appoets.com on 19-05-2018.
 */
public interface SocialIView extends MvpView {
    void onSuccess(Token token);

    void onError(Throwable e);
}
