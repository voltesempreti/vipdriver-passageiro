package com.vipdriver.user.ui.activity.location_pick;

import com.vipdriver.user.base.MvpPresenter;

public interface LocationPickIPresenter<V extends LocationPickIView> extends MvpPresenter<V> {
    void address();
}
