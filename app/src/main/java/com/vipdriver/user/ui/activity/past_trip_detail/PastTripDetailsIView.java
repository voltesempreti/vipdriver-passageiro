package com.vipdriver.user.ui.activity.past_trip_detail;

import com.vipdriver.user.base.MvpView;
import com.vipdriver.user.data.network.model.Datum;

import java.util.List;

public interface PastTripDetailsIView extends MvpView {

    void onSuccess(List<Datum> pastTripDetails);

    void onError(Throwable e);
}
