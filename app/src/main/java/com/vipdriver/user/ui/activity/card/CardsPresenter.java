package com.vipdriver.user.ui.activity.card;

import com.vipdriver.user.base.BasePresenter;
import com.vipdriver.user.data.network.APIClient;
import com.vipdriver.user.data.network.model.Card;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class CardsPresenter<V extends CardsIView> extends
        BasePresenter<V> implements CarsIPresenter<V> {

    @Override
    public void card() {
        getCompositeDisposable().add(APIClient.getAPIClient().card()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<Card>>() {
                               @Override
                               public void accept(List<Card> cards) throws Exception {
                                   CardsPresenter.this.getMvpView().onSuccess(cards);
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                CardsPresenter.this.getMvpView().onError(throwable);
                            }
                        }));
    }
}
