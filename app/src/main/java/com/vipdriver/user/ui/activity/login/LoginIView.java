package com.vipdriver.user.ui.activity.login;

import com.vipdriver.user.base.MvpView;
import com.vipdriver.user.data.network.model.ForgotResponse;
import com.vipdriver.user.data.network.model.Token;

public interface LoginIView extends MvpView {
    void onSuccess(Token token);

    void onSuccess(ForgotResponse object);

    void onError(Throwable e);
}
