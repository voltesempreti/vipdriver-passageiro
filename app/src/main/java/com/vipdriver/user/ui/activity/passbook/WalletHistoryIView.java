package com.vipdriver.user.ui.activity.passbook;

import com.vipdriver.user.base.MvpView;
import com.vipdriver.user.data.network.model.WalletResponse;

public interface WalletHistoryIView extends MvpView {
    void onSuccess(WalletResponse response);

    void onError(Throwable e);

}
