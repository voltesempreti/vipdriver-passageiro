package com.vipdriver.user.ui.fragment.dispute;

import com.vipdriver.user.base.MvpView;
import com.vipdriver.user.data.network.model.DisputeResponse;
import com.vipdriver.user.data.network.model.Help;

import java.util.List;

public interface DisputeIView extends MvpView {

    void onSuccess(Object object);

    void onSuccessDispute(List<DisputeResponse> responseList);

    void onError(Throwable e);

    void onSuccess(Help help);
}
