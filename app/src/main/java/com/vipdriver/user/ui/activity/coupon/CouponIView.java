package com.vipdriver.user.ui.activity.coupon;

import com.vipdriver.user.base.MvpView;
import com.vipdriver.user.data.network.model.PromoResponse;

public interface CouponIView extends MvpView {
    void onSuccess(PromoResponse object);

    void onError(Throwable e);
}
