package com.vipdriver.user.ui.activity.invite_friend;

import com.vipdriver.user.base.MvpPresenter;

public interface InviteFriendIPresenter<V extends InviteFriendIView> extends MvpPresenter<V> {
    void profile();
}
