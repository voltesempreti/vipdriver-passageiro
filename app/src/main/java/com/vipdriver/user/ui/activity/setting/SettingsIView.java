package com.vipdriver.user.ui.activity.setting;

import com.vipdriver.user.base.MvpView;
import com.vipdriver.user.data.network.model.AddressResponse;

public interface SettingsIView extends MvpView {

    void onSuccessAddress(Object object);

    void onLanguageChanged(Object object);

    void onSuccess(AddressResponse address);

    void onError(Throwable e);
}
