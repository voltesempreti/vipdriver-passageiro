package com.vipdriver.user.ui.activity.wallet;

import com.vipdriver.user.base.MvpView;
import com.vipdriver.user.data.network.model.AddWallet;
import com.vipdriver.user.data.network.model.WalletResponse;

public interface WalletIView extends MvpView {
    void onSuccess(AddWallet object);
    void onError(Throwable e);
    void onSuccess(WalletResponse response);
}
