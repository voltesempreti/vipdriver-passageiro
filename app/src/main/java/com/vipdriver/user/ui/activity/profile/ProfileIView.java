package com.vipdriver.user.ui.activity.profile;

import com.vipdriver.user.base.MvpView;
import com.vipdriver.user.data.network.model.User;

public interface ProfileIView extends MvpView {

    void onSuccess(User user);

    void onError(Throwable e);

}
