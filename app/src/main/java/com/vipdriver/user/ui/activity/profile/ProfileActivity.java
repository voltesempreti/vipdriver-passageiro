package com.vipdriver.user.ui.activity.profile;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import androidx.annotation.NonNull;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.vipdriver.user.BuildConfig;
import com.vipdriver.user.MvpApplication;
import com.vipdriver.user.R;
import com.vipdriver.user.base.BaseActivity;
import com.vipdriver.user.common.validator.ValidaCPF;
import com.vipdriver.user.data.SharedHelper;
import com.vipdriver.user.data.network.model.User;
import com.vipdriver.user.ui.activity.profile_update.ProfileUpdateActivity;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

import static com.vipdriver.user.data.SharedHelper.key.PROFILE_IMG;

public class ProfileActivity extends BaseActivity implements ProfileIView {

    @BindView(R.id.picture)
    CircularImageView picture;
    @BindView(R.id.completeData)
    LinearLayout completeData;
    @BindView(R.id.nameProfile)
    TextView name;
    @BindView(R.id.cpfProfile)
    TextView cpf;
    @BindView(R.id.mobileProfile)
    TextView mobile;
    @BindView(R.id.emailProfile)
    TextView email;
    File imgFile = null;
    @BindView(R.id.fabEdit)
    FloatingActionButton edit;

    public static final int REQUEST_PERMISSION = 100;
    private ProfilePresenter<ProfileActivity> profilePresenter = new ProfilePresenter<>();
    private AlertDialog mDialog;
    private String userAvatar;
    private ValidaCPF validaCPF;

    @Override
    public int getLayoutId() {
        return R.layout.activity_profile;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this);
        profilePresenter.attachView(this);
        setTitle(getString(R.string.profile));

        showLoading();
        profilePresenter.profile();
        Glide.with(baseActivity())
                .load(SharedHelper.getKey(baseActivity(), PROFILE_IMG))
                .apply(RequestOptions
                        .placeholderOf(R.drawable.ic_user_placeholder)
                        .dontAnimate()
                        .error(R.drawable.ic_user_placeholder))
                .into(picture);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, ProfileActivity.this, new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                imgFile = imageFiles.get(0);
                Glide.with(baseActivity())
                        .load(Uri.fromFile(imgFile))
                        .apply(RequestOptions
                                .placeholderOf(R.drawable.ic_user_placeholder)
                                .dontAnimate()
                                .error(R.drawable.ic_user_placeholder))
                        .into(picture);
            }
        });

    }


    @OnClick({R.id.fabEdit, R.id.mobileProfile})
    public void onViewClicked(View view) {

        switch (view.getId()) {
            case R.id.picture:
                if (hasPermission(Manifest.permission.CAMERA) && hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE))
                    pickImage();
                else
                    requestPermissionsSafely(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION);
                break;
            case R.id.fabEdit:
                Intent intent = new Intent(ProfileActivity.this, ProfileUpdateActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION:
                if (grantResults.length > 0) {
                    boolean permission1 = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean permission2 = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (permission1 && permission2) pickImage();
                    else
                        Toast.makeText(getApplicationContext(), R.string.please_give_permissions, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onSuccess(@NonNull User user) {
        try {
            hideLoading();
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        //Verifica se o usuário não informou o CPF ou se não enviou a foto de perfil
        if(user.getCpf() == null || user.getPicture() == null){
            completeData.setVisibility(View.VISIBLE);
        }

        //Seta avatar
        userAvatar = user.getPicture();
        String loginBy = user.getLoginBy();
        SharedHelper.putKey(this, "lang", user.getLanguage());
        name.setText(user.getFirstName() + " " + user.getLastName());
        cpf.setText("CPF: "+user.getCpf());
        mobile.setText("Número: "+ user.getMobile());
        email.setText("E-mail: "+ user.getEmail());
        SharedHelper.putKey(this, "stripe_publishable_key", user.getStripePublishableKey());
        SharedHelper.putKey(this, "measurementType", user.getMeasurement());
        Glide.with(baseActivity())
                .load(BuildConfig.BASE_IMAGE_URL + user.getPicture())
                .apply(RequestOptions
                        .placeholderOf(R.drawable.ic_user_placeholder)
                        .dontAnimate().error(R.drawable.ic_user_placeholder))
                .into(picture);
        MvpApplication.showOTP = user.getRide_otp().equals("1");
    }

    @Override
    public void onError(Throwable e) {
        handleError(e);
    }

    @Override
    protected void onDestroy() {
        profilePresenter.onDetach();
        super.onDestroy();
    }
}