package com.vipdriver.user.common.fcm;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Vibrator;
import android.provider.Settings;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.vipdriver.user.MvpApplication;
import com.vipdriver.user.R;
import com.vipdriver.user.common.Constants;
import com.vipdriver.user.data.SharedHelper;
import com.vipdriver.user.ui.activity.main.MainActivity;

@SuppressLint("InvalidWakeLockTag")
public class MyFireBaseMessagingService extends FirebaseMessagingService {

    private int notificationId = 0;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        @SuppressLint("HardwareIds")
        String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        SharedHelper.putKey(this, "device_token", s);
        SharedHelper.putKey(this, "device_id", deviceId);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        String chat = null;
        System.out.println("RRR FireBaseMessaging data payload: " + remoteMessage.getData());
        if (remoteMessage.getData().size() > 0) {
            try {
                chat = remoteMessage.getData().get("custom");
            } catch (Exception e) {
                e.printStackTrace();
            }
            openMainActivity(remoteMessage.getData().get("message"), !TextUtils.isEmpty(chat));
            sendBroadcast(new Intent(Constants.BroadcastReceiver.INTENT_FILTER));
        } else sendBroadcast(new Intent(Constants.BroadcastReceiver.INTENT_FILTER));
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void openMainActivity(String messageBody, boolean isChat) {
        System.out.println("RRR FireBaseMessaging messageBody = [" + messageBody + "], isChat = [" + isChat + "]");

        Log.i("ShowNotification", "Entrou");

        MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.notification_push);
        mediaPlayer.start();

        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(400);

        MvpApplication.canGoToChatScreen = isChat;
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pIntent = PendingIntent.getActivity
                (this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat
                .Builder(this, channelId)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody))
                .setSmallIcon(R.drawable.logo)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(uri)
                .setContentIntent(pIntent);

        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        AudioAttributes attributes = new AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .setUsage(AudioAttributes.USAGE_NOTIFICATION_EVENT)
                .build();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Log.i("ShowNotification", "Entrou 2");
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(messageBody);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
            channel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            channel.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION), attributes);
            channel.enableVibration(true);
            nm.createNotificationChannel(channel);
        }

        nm.notify(notificationId++, notificationBuilder.build());
    }
}
