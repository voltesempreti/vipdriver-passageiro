°B
(
&
androidx.appcompat	appcompat*1.1.0
*
(
androidx.annotation
annotation*1.1.0


androidx.corecore*1.1.0
0
.
androidx.cursoradaptercursoradapter*1.0.0
&
$
androidx.fragmentfragment*1.1.0
2
0
androidx.appcompatappcompat-resources*1.1.0
.
,
androidx.drawerlayoutdrawerlayout*1.0.0
*
(
androidx.collection
collection*1.1.0
0
.
androidx.lifecyclelifecycle-runtime*2.1.0
<
:
androidx.versionedparcelableversionedparcelable*1.1.0
/
-
androidx.lifecyclelifecycle-common*2.1.0
*
(
androidx.arch.corecore-common*2.1.0
(
&
androidx.viewpager	viewpager*1.0.0
"
 
androidx.loaderloader*1.0.0
&
$
androidx.activityactivity*1.0.0
2
0
androidx.lifecyclelifecycle-viewmodel*2.1.0
*
(
androidx.customview
customview*1.0.0
1
/
androidx.lifecyclelifecycle-livedata*2.1.0
+
)
androidx.arch.corecore-runtime*2.1.0
6
4
androidx.lifecyclelifecycle-livedata-core*2.1.0
*
(
androidx.savedstate
savedstate*1.0.0
2
0
androidx.vectordrawablevectordrawable*1.1.0
;
9
androidx.vectordrawablevectordrawable-animated*1.1.0
.
,
androidx.interpolatorinterpolator*1.0.0
-
+
androidx.legacylegacy-support-v4*1.0.0
 

androidx.mediamedia*1.0.0
5
3
androidx.legacylegacy-support-core-utils*1.0.0
2
0
androidx.legacylegacy-support-core-ui*1.0.0
.
,
androidx.documentfiledocumentfile*1.0.0
@
>
androidx.localbroadcastmanagerlocalbroadcastmanager*1.0.0
 

androidx.printprint*1.0.0
8
6
androidx.coordinatorlayoutcoordinatorlayout*1.1.0
8
6
androidx.slidingpanelayoutslidingpanelayout*1.0.0
:
8
androidx.swiperefreshlayoutswiperefreshlayout*1.0.0
<
:
androidx.asynclayoutinflaterasynclayoutinflater*1.0.0
0
.
com.google.android.materialmaterial*1.1.0
&
$
androidx.cardviewcardview*1.0.0
.
,
androidx.recyclerviewrecyclerview*1.1.0
*
(
androidx.transition
transition*1.2.0
*
(
androidx.viewpager2
viewpager2*1.0.0
6
4
androidx.constraintlayoutconstraintlayout*1.1.3
=
;
androidx.constraintlayoutconstraintlayout-solver*1.1.3
&
$
androidx.multidexmultidex*2.0.1
/
-
com.firebasefirebase-jobdispatcher*0.8.5
.
,
com.firebaseuifirebase-ui-storage*3.2.1
,
*
com.github.bumptech.glideglide*4.10.0
1
/
com.google.firebasefirebase-storage*19.1.1
1
/
com.github.bumptech.glide
gifdecoder*4.10.0
3
1
com.github.bumptech.glidedisklrucache*4.10.0
2
0
com.github.bumptech.glideannotations*4.10.0
6
4
com.google.android.gmsplay-services-base*17.0.0
7
5
com.google.android.gmsplay-services-tasks*17.0.0
6
4
com.google.firebasefirebase-auth-interop*18.0.0
0
.
com.google.firebasefirebase-common*19.3.0
4
2
com.google.firebasefirebase-components*16.0.0
:
8
com.google.android.gmsplay-services-basement*17.0.0
8
6
com.google.auto.valueauto-value-annotations*1.6.5
3
1
com.google.firebasefirebase-messaging*20.1.0
:
8
 com.google.android.datatransporttransport-api*2.2.0
B
@
 com.google.android.datatransporttransport-backend-cct*2.2.0
>
<
 com.google.android.datatransporttransport-runtime*2.2.0
7
5
com.google.firebasefirebase-datatransport*17.0.3
7
5
com.google.firebasefirebase-encoders-json*16.0.0
-
+
com.google.firebasefirebase-iid*20.0.2
?
=
com.google.firebasefirebase-measurement-connector*18.0.0
#
!
com.google.daggerdagger*2.24
!

javax.injectjavax.inject*1
7
5
com.google.android.gmsplay-services-stats*17.0.0
5
3
com.google.firebasefirebase-iid-interop*17.0.0
2
0
com.google.firebasefirebase-database*19.2.1
=
;
com.google.firebasefirebase-database-collection*17.0.0
.
,
com.google.firebasefirebase-core*17.2.2
3
1
com.google.firebasefirebase-analytics*17.2.2
=
;
com.google.android.gmsplay-services-measurement*17.2.2
A
?
com.google.android.gmsplay-services-measurement-api*17.2.2
A
?
com.google.android.gmsplay-services-measurement-sdk*17.2.2
B
@
com.google.android.gmsplay-services-measurement-base*17.2.2
B
@
com.google.android.gmsplay-services-measurement-impl*17.2.2
@
>
com.google.android.gmsplay-services-ads-identifier*17.0.0
E
C
com.google.android.gms!play-services-measurement-sdk-api*17.2.2
0
.
com.facebook.androidfacebook-login*4.38.1
/
-
com.facebook.androidfacebook-core*4.38.1
1
/
com.facebook.androidfacebook-common*4.38.1
)
'
com.parse.boltsbolts-android*1.4.0
'
%
com.parse.boltsbolts-tasks*1.4.0
*
(
com.parse.boltsbolts-applinks*1.4.0
!

com.google.zxingcore*3.3.0
$
"
androidx.browserbrowser*1.0.0
1
/
com.facebook.androidaccount-kit-sdk*4.37.0
@
>
com.google.android.gmsplay-services-auth-api-phone*17.0.0
6
4
com.google.android.gmsplay-services-auth*17.0.0
9
7
com.googlecode.libphonenumberlibphonenumber*8.9.10
;
9
com.google.android.gmsplay-services-auth-base*17.0.0
(
&
com.facebook.shimmershimmer*0.4.0
.
,
com.facebook.stethostetho-okhttp3*1.5.1
&
$
com.facebook.stethostetho*1.5.1
+
)
com.google.code.findbugsjsr305*2.0.1
'
%
com.squareup.okhttp3okhttp*4.2.2
!

commons-clicommons-cli*1.2
"
 
com.squareup.okiookio*2.2.2
/
-
org.jetbrains.kotlinkotlin-stdlib*1.3.50
6
4
org.jetbrains.kotlinkotlin-stdlib-common*1.3.50
$
"
org.jetbrainsannotations*13.0
6
4
com.google.maps.androidandroid-maps-utils*0.6.2
6
4
com.google.android.gmsplay-services-maps*17.0.0
:
8
com.google.android.gmsplay-services-location*17.0.0
D
B
com.google.android.gms play-services-places-placereport*17.0.0
6
4
#com.google.android.libraries.placesplaces*2.2.0
3
1
androidx.lifecyclelifecycle-extensions*2.1.0
%
#
com.android.volleyvolley*1.1.1
%
#
com.google.code.gsongson*2.8.5
0
.
androidx.lifecyclelifecycle-process*2.1.0
0
.
androidx.lifecyclelifecycle-service*2.1.0
'
%
io.reactivex.rxjava2rxjava*2.2.2
0
.
org.reactivestreamsreactive-streams*1.0.3
*
(
io.reactivex.rxjava2	rxandroid*2.1.0
+
)
com.squareup.retrofit2retrofit*2.7.0
1
/
com.squareup.retrofit2converter-gson*2.7.0
2
0
com.squareup.retrofit2adapter-rxjava2*2.7.0
4
2
com.squareup.okhttp3logging-interceptor*4.2.2
(
&
com.jakewhartonbutterknife*10.2.0
0
.
com.jakewhartonbutterknife-runtime*10.2.0
4
2
com.jakewhartonbutterknife-annotations*10.2.0
(
&
de.hdodenhofcircleimageview*3.0.1
%
#
com.airbnb.androidlottie*3.4.1
*
(
com.github.jkwiecien	EasyImage*2.0.4
1
/
com.akexorcistgoogledirectionlibrary*1.1.1
=
;
me.himanshusoni.chatmessageviewchat-message-view*1.0.3
&
$
com.intuit.sdpsdp-android*1.0.5
&
$
com.intuit.sspssp-android*1.0.6
%
#
com.daimajia.easinglibrary*2.1
0
.
com.daimajia.androidanimationslibrary*2.3
4
2
com.crashlytics.sdk.androidcrashlytics*2.10.1
8
6
com.crashlytics.sdk.androidcrashlytics-core*2.7.0
-
+
com.crashlytics.sdk.androidbeta*1.2.10
(
&
io.fabric.sdk.androidfabric*1.4.8
/
-
com.crashlytics.sdk.androidanswers*1.4.7
%
#

com.stripestripe-android*8.0.0
&
$
com.github.GrenderGToasty*1.3.1
 

	id.zelory
compressor*2.1.0
.
,
com.mikhaellopezcircularimageview*3.2.0
&
$

com.balysvmaterial-ripple*1.0.2
:
8
com.ryanjeffreybrooksindefinitepagerindicator*1.0.9
4
2
org.jetbrains.kotlinkotlin-stdlib-jdk7*1.2.41		

	
	
			
 !" !"# $%&'$%&	'%())*+,-.	-/01/01
.23456	2737374273557386869:;<7356=>?@:;:<><:AABB>	=:;<5?	7C356DC7D27@7E273456FFGHHIJK
I7LMCL7
MN7LCN7J	7LMO356?@O7LK7LMPQR QSSTUTUT
RQV $WV	W
XYZ[ #Y273ZY\273\273[]^_`a_b`b`acdcddefefgh27i27j3j7k $l%m:;<27ih38nl	
opopmnqrrsqtautnvtqrwaxyyzz{| c}	~twuhg	#  q	 %dA
base9 EGPXZ]^a#$g(hi*+k,-.qstuvwx9{|}~