/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.vipdriver.user;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.vipdriver.user";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 9;
  public static final String VERSION_NAME = "1.0.9";
  // Fields from default config.
  public static final String BASE_IMAGE_URL = "https://vipdriver.app/storage/";
  public static final String BASE_PAY_URL = "https://vipdriver.app/index.php";
  public static final String BASE_URL = "https://vipdriver.app/";
  public static final String CLIENT_ID = "2";
  public static final String CLIENT_SECRET = "taHdKq3goXAkwavriUFZQVXbHag1AxyJseNCVgiE";
  public static final String DEVICE_TYPE = "android";
  public static final String DRIVER_PACKAGE = "com.vipdriver.driver";
  public static final String FCM_SERRVER_KEY = "AAAAzhzfnX8:APA91bENNpNiFiy2UametRyA5I5zJtl1GxzvSfdjATzNE3tRdrwUiCrqF1kqMvaTyV9PrK1giYbIcQ7vyIe9eGcNA_c8l9ftspZmZgzodp3mRzLKLc0jbjPLziUhXLnJFCCU05QoIOoc";
  public static final String HELP_URL = "https://vipdriver.app/help";
  public static final String PAYPAL_CLIENT_TOKEN = "123";
  public static final String TERMS_CONDITIONS = "https://vipdriver.app/privacy";
}
