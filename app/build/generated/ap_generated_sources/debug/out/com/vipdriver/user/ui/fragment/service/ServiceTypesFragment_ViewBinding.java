// Generated code from Butter Knife. Do not modify!
package com.vipdriver.user.ui.fragment.service;

import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.vipdriver.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ServiceTypesFragment_ViewBinding implements Unbinder {
  private ServiceTypesFragment target;

  private View view7f0a0200;

  private View view7f0a013a;

  private View view7f0a0251;

  private View view7f0a023b;

  @UiThread
  public ServiceTypesFragment_ViewBinding(final ServiceTypesFragment target, View source) {
    this.target = target;

    View view;
    target.serviceRv = Utils.findRequiredViewAsType(source, R.id.service_rv, "field 'serviceRv'", RecyclerView.class);
    target.capacity = Utils.findRequiredViewAsType(source, R.id.capacity, "field 'capacity'", TextView.class);
    view = Utils.findRequiredView(source, R.id.payment_type, "field 'paymentType' and method 'onViewClicked'");
    target.paymentType = Utils.castView(view, R.id.payment_type, "field 'paymentType'", TextView.class);
    view7f0a0200 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.errorLayout = Utils.findRequiredViewAsType(source, R.id.error_layout, "field 'errorLayout'", TextView.class);
    target.useWallet = Utils.findRequiredViewAsType(source, R.id.use_wallet, "field 'useWallet'", CheckBox.class);
    target.walletBalance = Utils.findRequiredViewAsType(source, R.id.wallet_balance, "field 'walletBalance'", TextView.class);
    target.surgeValue = Utils.findRequiredViewAsType(source, R.id.surge_value, "field 'surgeValue'", TextView.class);
    target.tvDemand = Utils.findRequiredViewAsType(source, R.id.tv_demand, "field 'tvDemand'", TextView.class);
    view = Utils.findRequiredView(source, R.id.get_pricing, "field 'get_princing' and method 'onViewClicked'");
    target.get_princing = Utils.castView(view, R.id.get_pricing, "field 'get_princing'", Button.class);
    view7f0a013a = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.schedule_ride, "method 'onViewClicked'");
    view7f0a0251 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.ride_now, "method 'onViewClicked'");
    view7f0a023b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ServiceTypesFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.serviceRv = null;
    target.capacity = null;
    target.paymentType = null;
    target.errorLayout = null;
    target.useWallet = null;
    target.walletBalance = null;
    target.surgeValue = null;
    target.tvDemand = null;
    target.get_princing = null;

    view7f0a0200.setOnClickListener(null);
    view7f0a0200 = null;
    view7f0a013a.setOnClickListener(null);
    view7f0a013a = null;
    view7f0a0251.setOnClickListener(null);
    view7f0a0251 = null;
    view7f0a023b.setOnClickListener(null);
    view7f0a023b = null;
  }
}
