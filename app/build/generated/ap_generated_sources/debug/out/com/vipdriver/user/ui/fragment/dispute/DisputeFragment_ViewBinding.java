// Generated code from Butter Knife. Do not modify!
package com.vipdriver.user.ui.fragment.dispute;

import android.view.View;
import android.widget.EditText;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.vipdriver.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DisputeFragment_ViewBinding implements Unbinder {
  private DisputeFragment target;

  private View view7f0a00f5;

  private View view7f0a0296;

  private View view7f0a0163;

  @UiThread
  public DisputeFragment_ViewBinding(final DisputeFragment target, View source) {
    this.target = target;

    View view;
    target.cancelReason = Utils.findRequiredViewAsType(source, R.id.cancel_reason, "field 'cancelReason'", EditText.class);
    target.rcvReason = Utils.findRequiredViewAsType(source, R.id.rcvReason, "field 'rcvReason'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.dismiss, "method 'onViewClicked'");
    view7f0a00f5 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.submit, "method 'onViewClicked'");
    view7f0a0296 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.ivSupportCall, "method 'onViewClicked'");
    view7f0a0163 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    DisputeFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.cancelReason = null;
    target.rcvReason = null;

    view7f0a00f5.setOnClickListener(null);
    view7f0a00f5 = null;
    view7f0a0296.setOnClickListener(null);
    view7f0a0296 = null;
    view7f0a0163.setOnClickListener(null);
    view7f0a0163 = null;
  }
}
