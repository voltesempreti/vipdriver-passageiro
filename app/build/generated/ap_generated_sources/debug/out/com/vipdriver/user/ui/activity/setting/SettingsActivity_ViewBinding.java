// Generated code from Butter Knife. Do not modify!
package com.vipdriver.user.ui.activity.setting;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.vipdriver.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SettingsActivity_ViewBinding implements Unbinder {
  private SettingsActivity target;

  private View view7f0a014c;

  private View view7f0a0318;

  @UiThread
  public SettingsActivity_ViewBinding(SettingsActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SettingsActivity_ViewBinding(final SettingsActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.home_status, "field 'homeStatus' and method 'onViewClicked'");
    target.homeStatus = Utils.castView(view, R.id.home_status, "field 'homeStatus'", TextView.class);
    view7f0a014c = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.homeAddress = Utils.findRequiredViewAsType(source, R.id.home_address, "field 'homeAddress'", TextView.class);
    view = Utils.findRequiredView(source, R.id.work_status, "field 'workStatus' and method 'onViewClicked'");
    target.workStatus = Utils.castView(view, R.id.work_status, "field 'workStatus'", TextView.class);
    view7f0a0318 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.workAddress = Utils.findRequiredViewAsType(source, R.id.work_address, "field 'workAddress'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SettingsActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.homeStatus = null;
    target.homeAddress = null;
    target.workStatus = null;
    target.workAddress = null;

    view7f0a014c.setOnClickListener(null);
    view7f0a014c = null;
    view7f0a0318.setOnClickListener(null);
    view7f0a0318 = null;
  }
}
