// Generated code from Butter Knife. Do not modify!
package com.vipdriver.user.ui.fragment;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.vipdriver.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RateCardFragment_ViewBinding implements Unbinder {
  private RateCardFragment target;

  private View view7f0a00fb;

  @UiThread
  public RateCardFragment_ViewBinding(final RateCardFragment target, View source) {
    this.target = target;

    View view;
    target.image = Utils.findRequiredViewAsType(source, R.id.image, "field 'image'", ImageView.class);
    target.capacity = Utils.findRequiredViewAsType(source, R.id.capacity, "field 'capacity'", TextView.class);
    target.baseFare = Utils.findRequiredViewAsType(source, R.id.base_fare, "field 'baseFare'", TextView.class);
    target.fareType = Utils.findRequiredViewAsType(source, R.id.fare_type, "field 'fareType'", TextView.class);
    target.fareKm = Utils.findRequiredViewAsType(source, R.id.fare_km, "field 'fareKm'", TextView.class);
    target.tvFareDistance = Utils.findRequiredViewAsType(source, R.id.tvFareDistance, "field 'tvFareDistance'", TextView.class);
    view = Utils.findRequiredView(source, R.id.done, "method 'onViewClicked'");
    view7f0a00fb = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    RateCardFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.image = null;
    target.capacity = null;
    target.baseFare = null;
    target.fareType = null;
    target.fareKm = null;
    target.tvFareDistance = null;

    view7f0a00fb.setOnClickListener(null);
    view7f0a00fb = null;
  }
}
