// Generated code from Butter Knife. Do not modify!
package com.vipdriver.user.ui.activity.social;

import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.vipdriver.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SocialLoginActivity_ViewBinding implements Unbinder {
  private SocialLoginActivity target;

  private View view7f0a0126;

  @UiThread
  public SocialLoginActivity_ViewBinding(SocialLoginActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SocialLoginActivity_ViewBinding(final SocialLoginActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.facebook, "method 'onViewClicked'");
    view7f0a0126 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    target = null;


    view7f0a0126.setOnClickListener(null);
    view7f0a0126 = null;
  }
}
