// Generated code from Butter Knife. Do not modify!
package com.vipdriver.user.ui.fragment.service_flow;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.vipdriver.user.R;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ServiceFlowFragment_ViewBinding implements Unbinder {
  private ServiceFlowFragment target;

  private View view7f0a0072;

  private View view7f0a0270;

  private View view7f0a0071;

  private View view7f0a0088;

  private View view7f0a0283;

  @UiThread
  public ServiceFlowFragment_ViewBinding(final ServiceFlowFragment target, View source) {
    this.target = target;

    View view;
    target.otp = Utils.findRequiredViewAsType(source, R.id.otp, "field 'otp'", TextView.class);
    target.avatar = Utils.findRequiredViewAsType(source, R.id.avatar, "field 'avatar'", CircleImageView.class);
    target.firstName = Utils.findRequiredViewAsType(source, R.id.first_name, "field 'firstName'", TextView.class);
    target.status = Utils.findRequiredViewAsType(source, R.id.status, "field 'status'", TextView.class);
    target.rating = Utils.findRequiredViewAsType(source, R.id.rating, "field 'rating'", RatingBar.class);
    view = Utils.findRequiredView(source, R.id.cancel, "field 'cancel' and method 'onViewClicked'");
    target.cancel = Utils.castView(view, R.id.cancel, "field 'cancel'", Button.class);
    view7f0a0072 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.share_ride, "field 'sharedRide' and method 'onViewClicked'");
    target.sharedRide = Utils.castView(view, R.id.share_ride, "field 'sharedRide'", Button.class);
    view7f0a0270 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.image = Utils.findRequiredViewAsType(source, R.id.image, "field 'image'", ImageView.class);
    target.serviceTypeName = Utils.findRequiredViewAsType(source, R.id.service_type_name, "field 'serviceTypeName'", TextView.class);
    target.serviceNumber = Utils.findRequiredViewAsType(source, R.id.service_number, "field 'serviceNumber'", TextView.class);
    target.serviceModel = Utils.findRequiredViewAsType(source, R.id.service_model, "field 'serviceModel'", TextView.class);
    view = Utils.findRequiredView(source, R.id.call, "field 'call' and method 'onViewClicked'");
    target.call = Utils.castView(view, R.id.call, "field 'call'", Button.class);
    view7f0a0071 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.chat, "field 'chat' and method 'onViewClicked'");
    target.chat = Utils.castView(view, R.id.chat, "field 'chat'", FloatingActionButton.class);
    view7f0a0088 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.providerEta = Utils.findRequiredViewAsType(source, R.id.provider_eta, "field 'providerEta'", TextView.class);
    view = Utils.findRequiredView(source, R.id.sos, "method 'onViewClicked'");
    view7f0a0283 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ServiceFlowFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.otp = null;
    target.avatar = null;
    target.firstName = null;
    target.status = null;
    target.rating = null;
    target.cancel = null;
    target.sharedRide = null;
    target.image = null;
    target.serviceTypeName = null;
    target.serviceNumber = null;
    target.serviceModel = null;
    target.call = null;
    target.chat = null;
    target.providerEta = null;

    view7f0a0072.setOnClickListener(null);
    view7f0a0072 = null;
    view7f0a0270.setOnClickListener(null);
    view7f0a0270 = null;
    view7f0a0071.setOnClickListener(null);
    view7f0a0071 = null;
    view7f0a0088.setOnClickListener(null);
    view7f0a0088 = null;
    view7f0a0283.setOnClickListener(null);
    view7f0a0283 = null;
  }
}
