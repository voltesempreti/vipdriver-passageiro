// Generated code from Butter Knife. Do not modify!
package com.vipdriver.user.ui.activity.profile;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.vipdriver.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProfileActivity_ViewBinding implements Unbinder {
  private ProfileActivity target;

  private View view7f0a01aa;

  private View view7f0a0125;

  @UiThread
  public ProfileActivity_ViewBinding(ProfileActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ProfileActivity_ViewBinding(final ProfileActivity target, View source) {
    this.target = target;

    View view;
    target.picture = Utils.findRequiredViewAsType(source, R.id.picture, "field 'picture'", CircularImageView.class);
    target.completeData = Utils.findRequiredViewAsType(source, R.id.completeData, "field 'completeData'", LinearLayout.class);
    target.name = Utils.findRequiredViewAsType(source, R.id.nameProfile, "field 'name'", TextView.class);
    target.cpf = Utils.findRequiredViewAsType(source, R.id.cpfProfile, "field 'cpf'", TextView.class);
    view = Utils.findRequiredView(source, R.id.mobileProfile, "field 'mobile' and method 'onViewClicked'");
    target.mobile = Utils.castView(view, R.id.mobileProfile, "field 'mobile'", TextView.class);
    view7f0a01aa = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.email = Utils.findRequiredViewAsType(source, R.id.emailProfile, "field 'email'", TextView.class);
    view = Utils.findRequiredView(source, R.id.fabEdit, "field 'edit' and method 'onViewClicked'");
    target.edit = Utils.castView(view, R.id.fabEdit, "field 'edit'", FloatingActionButton.class);
    view7f0a0125 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ProfileActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.picture = null;
    target.completeData = null;
    target.name = null;
    target.cpf = null;
    target.mobile = null;
    target.email = null;
    target.edit = null;

    view7f0a01aa.setOnClickListener(null);
    view7f0a01aa = null;
    view7f0a0125.setOnClickListener(null);
    view7f0a0125 = null;
  }
}
