// Generated code from Butter Knife. Do not modify!
package com.vipdriver.user.ui.fragment.schedule;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.vipdriver.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ScheduleFragment_ViewBinding implements Unbinder {
  private ScheduleFragment target;

  private View view7f0a00e0;

  private View view7f0a02b3;

  private View view7f0a0250;

  @UiThread
  public ScheduleFragment_ViewBinding(final ScheduleFragment target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.date, "field 'date' and method 'onViewClicked'");
    target.date = Utils.castView(view, R.id.date, "field 'date'", TextView.class);
    view7f0a00e0 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.time, "field 'time' and method 'onViewClicked'");
    target.time = Utils.castView(view, R.id.time, "field 'time'", TextView.class);
    view7f0a02b3 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.schedule_request, "method 'onViewClicked'");
    view7f0a0250 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ScheduleFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.date = null;
    target.time = null;

    view7f0a00e0.setOnClickListener(null);
    view7f0a00e0 = null;
    view7f0a02b3.setOnClickListener(null);
    view7f0a02b3 = null;
    view7f0a0250.setOnClickListener(null);
    view7f0a0250 = null;
  }
}
