// Generated code from Butter Knife. Do not modify!
package com.vipdriver.user.ui.fragment.cancel_ride;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.vipdriver.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CancelRideDialogFragment_ViewBinding implements Unbinder {
  private CancelRideDialogFragment target;

  private View view7f0a00f5;

  private View view7f0a0296;

  @UiThread
  public CancelRideDialogFragment_ViewBinding(final CancelRideDialogFragment target, View source) {
    this.target = target;

    View view;
    target.cancelReason = Utils.findRequiredViewAsType(source, R.id.cancel_reason, "field 'cancelReason'", EditText.class);
    view = Utils.findRequiredView(source, R.id.dismiss, "field 'dismiss' and method 'onViewClicked'");
    target.dismiss = Utils.castView(view, R.id.dismiss, "field 'dismiss'", Button.class);
    view7f0a00f5 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.submit, "field 'submit' and method 'onViewClicked'");
    target.submit = Utils.castView(view, R.id.submit, "field 'submit'", Button.class);
    view7f0a0296 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rcvReason = Utils.findRequiredViewAsType(source, R.id.rcvReason, "field 'rcvReason'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CancelRideDialogFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.cancelReason = null;
    target.dismiss = null;
    target.submit = null;
    target.rcvReason = null;

    view7f0a00f5.setOnClickListener(null);
    view7f0a00f5 = null;
    view7f0a0296.setOnClickListener(null);
    view7f0a0296 = null;
  }
}
