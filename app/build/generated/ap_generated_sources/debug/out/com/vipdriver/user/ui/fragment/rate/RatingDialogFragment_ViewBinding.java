// Generated code from Butter Knife. Do not modify!
package com.vipdriver.user.ui.fragment.rate;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.vipdriver.user.R;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RatingDialogFragment_ViewBinding implements Unbinder {
  private RatingDialogFragment target;

  private View view7f0a0296;

  @UiThread
  public RatingDialogFragment_ViewBinding(final RatingDialogFragment target, View source) {
    this.target = target;

    View view;
    target.avatar = Utils.findRequiredViewAsType(source, R.id.avatar, "field 'avatar'", CircleImageView.class);
    target.rating = Utils.findRequiredViewAsType(source, R.id.rating, "field 'rating'", RatingBar.class);
    target.comment = Utils.findRequiredViewAsType(source, R.id.comment, "field 'comment'", EditText.class);
    view = Utils.findRequiredView(source, R.id.submit, "field 'submit' and method 'onViewClicked'");
    target.submit = Utils.castView(view, R.id.submit, "field 'submit'", Button.class);
    view7f0a0296 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
    target.ratingsName = Utils.findRequiredViewAsType(source, R.id.ratings_name, "field 'ratingsName'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RatingDialogFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.avatar = null;
    target.rating = null;
    target.comment = null;
    target.submit = null;
    target.ratingsName = null;

    view7f0a0296.setOnClickListener(null);
    view7f0a0296 = null;
  }
}
