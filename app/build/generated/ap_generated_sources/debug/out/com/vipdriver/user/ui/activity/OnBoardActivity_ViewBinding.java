// Generated code from Butter Knife. Do not modify!
package com.vipdriver.user.ui.activity;

import android.view.View;
import android.widget.LinearLayout;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.viewpager.widget.ViewPager;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.vipdriver.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OnBoardActivity_ViewBinding implements Unbinder {
  private OnBoardActivity target;

  private View view7f0a0277;

  private View view7f0a0278;

  private View view7f0a0281;

  @UiThread
  public OnBoardActivity_ViewBinding(OnBoardActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public OnBoardActivity_ViewBinding(final OnBoardActivity target, View source) {
    this.target = target;

    View view;
    target.viewPager = Utils.findRequiredViewAsType(source, R.id.view_pager, "field 'viewPager'", ViewPager.class);
    target.layoutDots = Utils.findRequiredViewAsType(source, R.id.layoutDots, "field 'layoutDots'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.sign_in, "method 'onViewClicked'");
    view7f0a0277 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.sign_up, "method 'onViewClicked'");
    view7f0a0278 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.social_login, "method 'onViewClicked'");
    view7f0a0281 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    OnBoardActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.viewPager = null;
    target.layoutDots = null;

    view7f0a0277.setOnClickListener(null);
    view7f0a0277 = null;
    view7f0a0278.setOnClickListener(null);
    view7f0a0278 = null;
    view7f0a0281.setOnClickListener(null);
    view7f0a0281 = null;
  }
}
