// Generated code from Butter Knife. Do not modify!
package com.vipdriver.user.ui.activity.passbook;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.vipdriver.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class WalletHistoryActivity_ViewBinding implements Unbinder {
  private WalletHistoryActivity target;

  @UiThread
  public WalletHistoryActivity_ViewBinding(WalletHistoryActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public WalletHistoryActivity_ViewBinding(WalletHistoryActivity target, View source) {
    this.target = target;

    target.rvWallet = Utils.findRequiredViewAsType(source, R.id.rvWallet, "field 'rvWallet'", RecyclerView.class);
    target.tvNoWalletData = Utils.findRequiredViewAsType(source, R.id.tvNoWalletData, "field 'tvNoWalletData'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    WalletHistoryActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rvWallet = null;
    target.tvNoWalletData = null;
  }
}
