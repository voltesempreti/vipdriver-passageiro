// Generated code from Butter Knife. Do not modify!
package com.vipdriver.user.ui.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.vipdriver.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class InvoiceDialogFragment_ViewBinding implements Unbinder {
  private InvoiceDialogFragment target;

  private View view7f0a0094;

  @UiThread
  public InvoiceDialogFragment_ViewBinding(final InvoiceDialogFragment target, View source) {
    this.target = target;

    View view;
    target.bookingId = Utils.findRequiredViewAsType(source, R.id.booking_id, "field 'bookingId'", TextView.class);
    target.distance = Utils.findRequiredViewAsType(source, R.id.distance, "field 'distance'", TextView.class);
    target.travelTime = Utils.findRequiredViewAsType(source, R.id.travel_time, "field 'travelTime'", TextView.class);
    target.fixed = Utils.findRequiredViewAsType(source, R.id.fixed, "field 'fixed'", TextView.class);
    target.distanceFare = Utils.findRequiredViewAsType(source, R.id.distance_fare, "field 'distanceFare'", TextView.class);
    target.tax = Utils.findRequiredViewAsType(source, R.id.tax, "field 'tax'", TextView.class);
    target.total = Utils.findRequiredViewAsType(source, R.id.total, "field 'total'", TextView.class);
    target.payable = Utils.findRequiredViewAsType(source, R.id.payable, "field 'payable'", TextView.class);
    view = Utils.findRequiredView(source, R.id.close, "field 'close' and method 'onViewClicked'");
    target.close = Utils.castView(view, R.id.close, "field 'close'", Button.class);
    view7f0a0094 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
    target.timeFare = Utils.findRequiredViewAsType(source, R.id.time_fare, "field 'timeFare'", TextView.class);
    target.tips = Utils.findRequiredViewAsType(source, R.id.tips, "field 'tips'", TextView.class);
    target.tipsLayout = Utils.findRequiredViewAsType(source, R.id.tips_layout, "field 'tipsLayout'", LinearLayout.class);
    target.distanceConstainer = Utils.findRequiredViewAsType(source, R.id.distance_constainer, "field 'distanceConstainer'", LinearLayout.class);
    target.timeContainer = Utils.findRequiredViewAsType(source, R.id.time_container, "field 'timeContainer'", LinearLayout.class);
    target.walletDeduction = Utils.findRequiredViewAsType(source, R.id.wallet_deduction, "field 'walletDeduction'", TextView.class);
    target.discount = Utils.findRequiredViewAsType(source, R.id.discount, "field 'discount'", TextView.class);
    target.walletLayout = Utils.findRequiredViewAsType(source, R.id.walletLayout, "field 'walletLayout'", LinearLayout.class);
    target.discountLayout = Utils.findRequiredViewAsType(source, R.id.discountLayout, "field 'discountLayout'", LinearLayout.class);
    target.llWaitingAmountContainer = Utils.findRequiredViewAsType(source, R.id.llWaitingAmountContainer, "field 'llWaitingAmountContainer'", LinearLayout.class);
    target.llTolleChargeContainer = Utils.findRequiredViewAsType(source, R.id.llTolleChargeContainer, "field 'llTolleChargeContainer'", LinearLayout.class);
    target.llRoundOffContainer = Utils.findRequiredViewAsType(source, R.id.llRoundOffContainer, "field 'llRoundOffContainer'", LinearLayout.class);
    target.tvWaitingAmount = Utils.findRequiredViewAsType(source, R.id.tvWaitingAmount, "field 'tvWaitingAmount'", TextView.class);
    target.tvTollCharges = Utils.findRequiredViewAsType(source, R.id.tvTollCharges, "field 'tvTollCharges'", TextView.class);
    target.tvRoundOff = Utils.findRequiredViewAsType(source, R.id.tvRoundOff, "field 'tvRoundOff'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    InvoiceDialogFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.bookingId = null;
    target.distance = null;
    target.travelTime = null;
    target.fixed = null;
    target.distanceFare = null;
    target.tax = null;
    target.total = null;
    target.payable = null;
    target.close = null;
    target.timeFare = null;
    target.tips = null;
    target.tipsLayout = null;
    target.distanceConstainer = null;
    target.timeContainer = null;
    target.walletDeduction = null;
    target.discount = null;
    target.walletLayout = null;
    target.discountLayout = null;
    target.llWaitingAmountContainer = null;
    target.llTolleChargeContainer = null;
    target.llRoundOffContainer = null;
    target.tvWaitingAmount = null;
    target.tvTollCharges = null;
    target.tvRoundOff = null;

    view7f0a0094.setOnClickListener(null);
    view7f0a0094 = null;
  }
}
