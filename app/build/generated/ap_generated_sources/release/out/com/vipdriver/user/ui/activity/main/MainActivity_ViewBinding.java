// Generated code from Butter Knife. Do not modify!
package com.vipdriver.user.ui.activity.main;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.drawerlayout.widget.DrawerLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.vipdriver.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MainActivity_ViewBinding implements Unbinder {
  private MainActivity target;

  private View view7f0a01a4;

  private View view7f0a015d;

  private View view7f0a0143;

  private View view7f0a0284;

  private View view7f0a00ef;

  private View view7f0a0085;

  private View view7f0a0184;

  private View view7f0a0185;

  private View view7f0a0293;

  @UiThread
  public MainActivity_ViewBinding(MainActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MainActivity_ViewBinding(final MainActivity target, View source) {
    this.target = target;

    View view;
    target.llChangeLocation = Utils.findRequiredViewAsType(source, R.id.llChangeLocation, "field 'llChangeLocation'", LinearLayout.class);
    target.lnrDelivery = Utils.findRequiredViewAsType(source, R.id.lnrDelivery, "field 'lnrDelivery'", LinearLayout.class);
    target.container = Utils.findRequiredViewAsType(source, R.id.container, "field 'container'", FrameLayout.class);
    view = Utils.findRequiredView(source, R.id.menu, "field 'menu' and method 'onViewClicked'");
    target.menu = Utils.castView(view, R.id.menu, "field 'menu'", ImageView.class);
    view7f0a01a4 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.ivBack, "field 'ivBack' and method 'onViewClicked'");
    target.ivBack = Utils.castView(view, R.id.ivBack, "field 'ivBack'", ImageView.class);
    view7f0a015d = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.gps, "field 'gps' and method 'onViewClicked'");
    target.gps = Utils.castView(view, R.id.gps, "field 'gps'", ImageView.class);
    view7f0a0143 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.source, "field 'sourceTxt' and method 'onViewClicked'");
    target.sourceTxt = Utils.castView(view, R.id.source, "field 'sourceTxt'", TextView.class);
    view7f0a0284 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.destinationTxt = Utils.findRequiredViewAsType(source, R.id.destination, "field 'destinationTxt'", TextView.class);
    view = Utils.findRequiredView(source, R.id.destination_welcome, "field 'destinationWelcome' and method 'onViewClicked'");
    target.destinationWelcome = Utils.castView(view, R.id.destination_welcome, "field 'destinationWelcome'", TextView.class);
    view7f0a00ef = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.drawerLayout = Utils.findRequiredViewAsType(source, R.id.drawer_layout, "field 'drawerLayout'", DrawerLayout.class);
    target.topLayout = Utils.findRequiredViewAsType(source, R.id.top_layout, "field 'topLayout'", LinearLayout.class);
    target.pickLocationLayout = Utils.findRequiredViewAsType(source, R.id.pick_location_layout, "field 'pickLocationLayout'", LinearLayout.class);
    target.welcome = Utils.findRequiredViewAsType(source, R.id.welcome_main, "field 'welcome'", TextView.class);
    view = Utils.findRequiredView(source, R.id.changeDestination, "field 'changeDestination' and method 'onViewClicked'");
    target.changeDestination = Utils.castView(view, R.id.changeDestination, "field 'changeDestination'", TextView.class);
    view7f0a0085 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llPickHomeAdd, "field 'llPickHomeAdd' and method 'onViewClicked'");
    target.llPickHomeAdd = Utils.castView(view, R.id.llPickHomeAdd, "field 'llPickHomeAdd'", LinearLayout.class);
    view7f0a0184 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llPickWorkAdd, "field 'llPickWorkAdd' and method 'onViewClicked'");
    target.llPickWorkAdd = Utils.castView(view, R.id.llPickWorkAdd, "field 'llPickWorkAdd'", LinearLayout.class);
    view7f0a0185 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.llDropLocationContainer = Utils.findRequiredViewAsType(source, R.id.llDropLocationContainer, "field 'llDropLocationContainer'", LinearLayout.class);
    target.llDropLocationContainer2 = Utils.findRequiredViewAsType(source, R.id.llDropLocationContainer2, "field 'llDropLocationContainer2'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.style_map, "field 'styleMapButton' and method 'onViewClicked'");
    target.styleMapButton = Utils.castView(view, R.id.style_map, "field 'styleMapButton'", ImageView.class);
    view7f0a0293 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    MainActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.llChangeLocation = null;
    target.lnrDelivery = null;
    target.container = null;
    target.menu = null;
    target.ivBack = null;
    target.gps = null;
    target.sourceTxt = null;
    target.destinationTxt = null;
    target.destinationWelcome = null;
    target.drawerLayout = null;
    target.topLayout = null;
    target.pickLocationLayout = null;
    target.welcome = null;
    target.changeDestination = null;
    target.llPickHomeAdd = null;
    target.llPickWorkAdd = null;
    target.llDropLocationContainer = null;
    target.llDropLocationContainer2 = null;
    target.styleMapButton = null;

    view7f0a01a4.setOnClickListener(null);
    view7f0a01a4 = null;
    view7f0a015d.setOnClickListener(null);
    view7f0a015d = null;
    view7f0a0143.setOnClickListener(null);
    view7f0a0143 = null;
    view7f0a0284.setOnClickListener(null);
    view7f0a0284 = null;
    view7f0a00ef.setOnClickListener(null);
    view7f0a00ef = null;
    view7f0a0085.setOnClickListener(null);
    view7f0a0085 = null;
    view7f0a0184.setOnClickListener(null);
    view7f0a0184 = null;
    view7f0a0185.setOnClickListener(null);
    view7f0a0185 = null;
    view7f0a0293.setOnClickListener(null);
    view7f0a0293 = null;
  }
}
