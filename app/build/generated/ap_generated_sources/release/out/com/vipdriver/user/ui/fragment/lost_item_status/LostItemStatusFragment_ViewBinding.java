// Generated code from Butter Knife. Do not modify!
package com.vipdriver.user.ui.fragment.lost_item_status;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.vipdriver.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LostItemStatusFragment_ViewBinding implements Unbinder {
  private LostItemStatusFragment target;

  @UiThread
  public LostItemStatusFragment_ViewBinding(LostItemStatusFragment target, View source) {
    this.target = target;

    target.userDispute = Utils.findRequiredViewAsType(source, R.id.user_dispute, "field 'userDispute'", TextView.class);
    target.adminComment = Utils.findRequiredViewAsType(source, R.id.admin_comment, "field 'adminComment'", TextView.class);
    target.lostItemStatus = Utils.findRequiredViewAsType(source, R.id.lost_item_status, "field 'lostItemStatus'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    LostItemStatusFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.userDispute = null;
    target.adminComment = null;
    target.lostItemStatus = null;
  }
}
