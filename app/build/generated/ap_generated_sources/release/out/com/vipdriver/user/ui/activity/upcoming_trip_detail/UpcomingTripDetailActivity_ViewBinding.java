// Generated code from Butter Knife. Do not modify!
package com.vipdriver.user.ui.activity.upcoming_trip_detail;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.vipdriver.user.R;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class UpcomingTripDetailActivity_ViewBinding implements Unbinder {
  private UpcomingTripDetailActivity target;

  private View view7f0a0072;

  private View view7f0a0071;

  @UiThread
  public UpcomingTripDetailActivity_ViewBinding(UpcomingTripDetailActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public UpcomingTripDetailActivity_ViewBinding(final UpcomingTripDetailActivity target,
      View source) {
    this.target = target;

    View view;
    target.staticMap = Utils.findRequiredViewAsType(source, R.id.static_map, "field 'staticMap'", ImageView.class);
    target.bookingId = Utils.findRequiredViewAsType(source, R.id.booking_id, "field 'bookingId'", TextView.class);
    target.name = Utils.findRequiredViewAsType(source, R.id.name, "field 'name'", TextView.class);
    target.rating = Utils.findRequiredViewAsType(source, R.id.rating, "field 'rating'", RatingBar.class);
    target.avatar = Utils.findRequiredViewAsType(source, R.id.avatar, "field 'avatar'", CircleImageView.class);
    target.scheduleAt = Utils.findRequiredViewAsType(source, R.id.schedule_at, "field 'scheduleAt'", TextView.class);
    target.sAddress = Utils.findRequiredViewAsType(source, R.id.s_address, "field 'sAddress'", TextView.class);
    target.dAddress = Utils.findRequiredViewAsType(source, R.id.d_address, "field 'dAddress'", TextView.class);
    target.paymentMode = Utils.findRequiredViewAsType(source, R.id.payment_mode, "field 'paymentMode'", TextView.class);
    view = Utils.findRequiredView(source, R.id.cancel, "field 'cancel' and method 'onViewClicked'");
    target.cancel = Utils.castView(view, R.id.cancel, "field 'cancel'", Button.class);
    view7f0a0072 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.call, "field 'call' and method 'onViewClicked'");
    target.call = Utils.castView(view, R.id.call, "field 'call'", Button.class);
    view7f0a0071 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.paymentImage = Utils.findRequiredViewAsType(source, R.id.payment_image, "field 'paymentImage'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    UpcomingTripDetailActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.staticMap = null;
    target.bookingId = null;
    target.name = null;
    target.rating = null;
    target.avatar = null;
    target.scheduleAt = null;
    target.sAddress = null;
    target.dAddress = null;
    target.paymentMode = null;
    target.cancel = null;
    target.call = null;
    target.paymentImage = null;

    view7f0a0072.setOnClickListener(null);
    view7f0a0072 = null;
    view7f0a0071.setOnClickListener(null);
    view7f0a0071 = null;
  }
}
