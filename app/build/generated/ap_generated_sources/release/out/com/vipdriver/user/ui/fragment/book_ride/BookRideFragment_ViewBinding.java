// Generated code from Butter Knife. Do not modify!
package com.vipdriver.user.ui.fragment.book_ride;

import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.vipdriver.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class BookRideFragment_ViewBinding implements Unbinder {
  private BookRideFragment target;

  private View view7f0a023b;

  private View view7f0a0303;

  private View view7f0a02f2;

  @UiThread
  public BookRideFragment_ViewBinding(final BookRideFragment target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.ride_now, "field 'rideNow' and method 'onViewClicked'");
    target.rideNow = Utils.castView(view, R.id.ride_now, "field 'rideNow'", Button.class);
    view7f0a023b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.tvEstimatedFare = Utils.findRequiredViewAsType(source, R.id.tvEstimatedFare, "field 'tvEstimatedFare'", TextView.class);
    target.useWallet = Utils.findRequiredViewAsType(source, R.id.use_wallet, "field 'useWallet'", CheckBox.class);
    target.estimatedImage = Utils.findRequiredViewAsType(source, R.id.estimated_image, "field 'estimatedImage'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.view_coupons, "field 'viewCoupons' and method 'onViewClicked'");
    target.viewCoupons = Utils.castView(view, R.id.view_coupons, "field 'viewCoupons'", TextView.class);
    view7f0a0303 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.estimatedPaymentMode = Utils.findRequiredViewAsType(source, R.id.estimated_payment_mode, "field 'estimatedPaymentMode'", TextView.class);
    view = Utils.findRequiredView(source, R.id.tv_change, "field 'tvChange' and method 'onViewClicked'");
    target.tvChange = Utils.castView(view, R.id.tv_change, "field 'tvChange'", TextView.class);
    view7f0a02f2 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.walletBalance = Utils.findRequiredViewAsType(source, R.id.wallet_balance, "field 'walletBalance'", TextView.class);
    target.llEstimatedFareContainer = Utils.findRequiredViewAsType(source, R.id.llEstimatedFareContainer, "field 'llEstimatedFareContainer'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    BookRideFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rideNow = null;
    target.tvEstimatedFare = null;
    target.useWallet = null;
    target.estimatedImage = null;
    target.viewCoupons = null;
    target.estimatedPaymentMode = null;
    target.tvChange = null;
    target.walletBalance = null;
    target.llEstimatedFareContainer = null;

    view7f0a023b.setOnClickListener(null);
    view7f0a023b = null;
    view7f0a0303.setOnClickListener(null);
    view7f0a0303 = null;
    view7f0a02f2.setOnClickListener(null);
    view7f0a02f2 = null;
  }
}
