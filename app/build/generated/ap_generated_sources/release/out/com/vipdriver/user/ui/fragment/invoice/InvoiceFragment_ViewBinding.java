// Generated code from Butter Knife. Do not modify!
package com.vipdriver.user.ui.fragment.invoice;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.core.widget.NestedScrollView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.vipdriver.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class InvoiceFragment_ViewBinding implements Unbinder {
  private InvoiceFragment target;

  private View view7f0a01ff;

  private View view7f0a000a;

  private View view7f0a01f8;

  private View view7f0a00fb;

  private View view7f0a02e2;

  private View view7f0a02ea;

  private View view7f0a0160;

  @UiThread
  public InvoiceFragment_ViewBinding(final InvoiceFragment target, View source) {
    this.target = target;

    View view;
    target.containerScroll = Utils.findRequiredViewAsType(source, R.id.fragment_invoice, "field 'containerScroll'", NestedScrollView.class);
    view = Utils.findRequiredView(source, R.id.payment_mode, "field 'tvPaymentMode' and method 'onViewClicked'");
    target.tvPaymentMode = Utils.castView(view, R.id.payment_mode, "field 'tvPaymentMode'", TextView.class);
    view7f0a01ff = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.accept_virtual_change, "field 'acceptVirtualChange' and method 'onViewClicked'");
    target.acceptVirtualChange = Utils.castView(view, R.id.accept_virtual_change, "field 'acceptVirtualChange'", Button.class);
    view7f0a000a = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.pay_now, "field 'payNow' and method 'onViewClicked'");
    target.payNow = Utils.castView(view, R.id.pay_now, "field 'payNow'", Button.class);
    view7f0a01f8 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.done, "field 'done' and method 'onViewClicked'");
    target.done = Utils.castView(view, R.id.done, "field 'done'", Button.class);
    view7f0a00fb = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.bookingId = Utils.findRequiredViewAsType(source, R.id.booking_id, "field 'bookingId'", TextView.class);
    target.distance = Utils.findRequiredViewAsType(source, R.id.distance, "field 'distance'", TextView.class);
    target.travelTime = Utils.findRequiredViewAsType(source, R.id.travel_time, "field 'travelTime'", TextView.class);
    target.fixed = Utils.findRequiredViewAsType(source, R.id.fixed, "field 'fixed'", TextView.class);
    target.distanceFare = Utils.findRequiredViewAsType(source, R.id.distance_fare, "field 'distanceFare'", TextView.class);
    target.tax = Utils.findRequiredViewAsType(source, R.id.tax, "field 'tax'", TextView.class);
    target.total = Utils.findRequiredViewAsType(source, R.id.total, "field 'total'", TextView.class);
    target.payable = Utils.findRequiredViewAsType(source, R.id.payable, "field 'payable'", TextView.class);
    target.walletDetection = Utils.findRequiredViewAsType(source, R.id.wallet_detection, "field 'walletDetection'", TextView.class);
    target.timeFare = Utils.findRequiredViewAsType(source, R.id.time_fare, "field 'timeFare'", TextView.class);
    target.llDistanceFareContainer = Utils.findRequiredViewAsType(source, R.id.llDistanceFareContainer, "field 'llDistanceFareContainer'", LinearLayout.class);
    target.llTimeFareContainer = Utils.findRequiredViewAsType(source, R.id.llTimeFareContainer, "field 'llTimeFareContainer'", LinearLayout.class);
    target.llTipContainer = Utils.findRequiredViewAsType(source, R.id.llTipContainer, "field 'llTipContainer'", LinearLayout.class);
    target.llWalletDeductionContainer = Utils.findRequiredViewAsType(source, R.id.llWalletDeductionContainer, "field 'llWalletDeductionContainer'", LinearLayout.class);
    target.llDiscountContainer = Utils.findRequiredViewAsType(source, R.id.llDiscountContainer, "field 'llDiscountContainer'", LinearLayout.class);
    target.llPaymentContainer = Utils.findRequiredViewAsType(source, R.id.llPaymentContainer, "field 'llPaymentContainer'", LinearLayout.class);
    target.llTravelTime = Utils.findRequiredViewAsType(source, R.id.llTravelTime, "field 'llTravelTime'", LinearLayout.class);
    target.llBaseFare = Utils.findRequiredViewAsType(source, R.id.llBaseFare, "field 'llBaseFare'", LinearLayout.class);
    target.llPayable = Utils.findRequiredViewAsType(source, R.id.llPayable, "field 'llPayable'", LinearLayout.class);
    target.llWaitingAmountContainer = Utils.findRequiredViewAsType(source, R.id.llWaitingAmountContainer, "field 'llWaitingAmountContainer'", LinearLayout.class);
    target.llTolleChargeContainer = Utils.findRequiredViewAsType(source, R.id.llTolleChargeContainer, "field 'llTolleChargeContainer'", LinearLayout.class);
    target.llRoundOffContainer = Utils.findRequiredViewAsType(source, R.id.llRoundOffContainer, "field 'llRoundOffContainer'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.tvGiveTip, "field 'tvGiveTip' and method 'onViewClicked'");
    target.tvGiveTip = Utils.castView(view, R.id.tvGiveTip, "field 'tvGiveTip'", TextView.class);
    view7f0a02e2 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.tvTipAmt, "field 'tvTipAmt' and method 'onViewClicked'");
    target.tvTipAmt = Utils.castView(view, R.id.tvTipAmt, "field 'tvTipAmt'", TextView.class);
    view7f0a02ea = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.tvDiscount = Utils.findRequiredViewAsType(source, R.id.tvDiscount, "field 'tvDiscount'", TextView.class);
    target.tvWaitingAmount = Utils.findRequiredViewAsType(source, R.id.tvWaitingAmount, "field 'tvWaitingAmount'", TextView.class);
    target.tvTollCharges = Utils.findRequiredViewAsType(source, R.id.tvTollCharges, "field 'tvTollCharges'", TextView.class);
    target.tvRoundOff = Utils.findRequiredViewAsType(source, R.id.tvRoundOff, "field 'tvRoundOff'", TextView.class);
    target.tvWaitingTimeDesc = Utils.findRequiredViewAsType(source, R.id.tvWaitingTimeDesc, "field 'tvWaitingTimeDesc'", TextView.class);
    target.tvWaitingTime = Utils.findRequiredViewAsType(source, R.id.tvWaitingTime, "field 'tvWaitingTime'", TextView.class);
    view = Utils.findRequiredView(source, R.id.ivInvoice, "method 'onViewClicked'");
    view7f0a0160 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    InvoiceFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.containerScroll = null;
    target.tvPaymentMode = null;
    target.acceptVirtualChange = null;
    target.payNow = null;
    target.done = null;
    target.bookingId = null;
    target.distance = null;
    target.travelTime = null;
    target.fixed = null;
    target.distanceFare = null;
    target.tax = null;
    target.total = null;
    target.payable = null;
    target.walletDetection = null;
    target.timeFare = null;
    target.llDistanceFareContainer = null;
    target.llTimeFareContainer = null;
    target.llTipContainer = null;
    target.llWalletDeductionContainer = null;
    target.llDiscountContainer = null;
    target.llPaymentContainer = null;
    target.llTravelTime = null;
    target.llBaseFare = null;
    target.llPayable = null;
    target.llWaitingAmountContainer = null;
    target.llTolleChargeContainer = null;
    target.llRoundOffContainer = null;
    target.tvGiveTip = null;
    target.tvTipAmt = null;
    target.tvDiscount = null;
    target.tvWaitingAmount = null;
    target.tvTollCharges = null;
    target.tvRoundOff = null;
    target.tvWaitingTimeDesc = null;
    target.tvWaitingTime = null;

    view7f0a01ff.setOnClickListener(null);
    view7f0a01ff = null;
    view7f0a000a.setOnClickListener(null);
    view7f0a000a = null;
    view7f0a01f8.setOnClickListener(null);
    view7f0a01f8 = null;
    view7f0a00fb.setOnClickListener(null);
    view7f0a00fb = null;
    view7f0a02e2.setOnClickListener(null);
    view7f0a02e2 = null;
    view7f0a02ea.setOnClickListener(null);
    view7f0a02ea = null;
    view7f0a0160.setOnClickListener(null);
    view7f0a0160 = null;
  }
}
