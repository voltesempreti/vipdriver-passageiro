// Generated code from Butter Knife. Do not modify!
package com.vipdriver.user.ui.activity.payment;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.vipdriver.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PaymentActivity_ViewBinding implements Unbinder {
  private PaymentActivity target;

  private View view7f0a0046;

  private View view7f0a007f;

  private View view7f0a00d7;

  private View view7f0a00e4;

  @UiThread
  public PaymentActivity_ViewBinding(PaymentActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public PaymentActivity_ViewBinding(final PaymentActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.add_card, "field 'addCard' and method 'onViewClicked'");
    target.addCard = Utils.castView(view, R.id.add_card, "field 'addCard'", TextView.class);
    view7f0a0046 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.cash, "field 'tvCash' and method 'onViewClicked'");
    target.tvCash = Utils.castView(view, R.id.cash, "field 'tvCash'", TextView.class);
    view7f0a007f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.cardsRv = Utils.findRequiredViewAsType(source, R.id.cards_rv, "field 'cardsRv'", RecyclerView.class);
    target.llCardContainer = Utils.findRequiredViewAsType(source, R.id.llCardContainer, "field 'llCardContainer'", LinearLayout.class);
    target.llCashContainer = Utils.findRequiredViewAsType(source, R.id.llCashContainer, "field 'llCashContainer'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.credit_card, "field 'credit_card' and method 'onViewClicked'");
    target.credit_card = Utils.castView(view, R.id.credit_card, "field 'credit_card'", TextView.class);
    view7f0a00d7 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.debit_machine, "field 'debit_machine' and method 'onViewClicked'");
    target.debit_machine = Utils.castView(view, R.id.debit_machine, "field 'debit_machine'", TextView.class);
    view7f0a00e4 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    PaymentActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.addCard = null;
    target.tvCash = null;
    target.cardsRv = null;
    target.llCardContainer = null;
    target.llCashContainer = null;
    target.credit_card = null;
    target.debit_machine = null;

    view7f0a0046.setOnClickListener(null);
    view7f0a0046 = null;
    view7f0a007f.setOnClickListener(null);
    view7f0a007f = null;
    view7f0a00d7.setOnClickListener(null);
    view7f0a00d7 = null;
    view7f0a00e4.setOnClickListener(null);
    view7f0a00e4 = null;
  }
}
