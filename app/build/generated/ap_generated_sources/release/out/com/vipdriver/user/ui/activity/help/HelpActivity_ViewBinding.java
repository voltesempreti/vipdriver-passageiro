// Generated code from Butter Knife. Do not modify!
package com.vipdriver.user.ui.activity.help;

import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.vipdriver.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class HelpActivity_ViewBinding implements Unbinder {
  private HelpActivity target;

  private View view7f0a0071;

  private View view7f0a019c;

  private View view7f0a030d;

  @UiThread
  public HelpActivity_ViewBinding(HelpActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public HelpActivity_ViewBinding(final HelpActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.call, "method 'onViewClicked'");
    view7f0a0071 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.mail, "method 'onViewClicked'");
    view7f0a019c = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.web, "method 'onViewClicked'");
    view7f0a030d = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    target = null;


    view7f0a0071.setOnClickListener(null);
    view7f0a0071 = null;
    view7f0a019c.setOnClickListener(null);
    view7f0a019c = null;
    view7f0a030d.setOnClickListener(null);
    view7f0a030d = null;
  }
}
