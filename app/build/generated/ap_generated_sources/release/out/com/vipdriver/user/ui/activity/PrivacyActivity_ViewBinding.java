// Generated code from Butter Knife. Do not modify!
package com.vipdriver.user.ui.activity;

import android.view.View;
import android.webkit.WebView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.vipdriver.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PrivacyActivity_ViewBinding implements Unbinder {
  private PrivacyActivity target;

  @UiThread
  public PrivacyActivity_ViewBinding(PrivacyActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public PrivacyActivity_ViewBinding(PrivacyActivity target, View source) {
    this.target = target;

    target.webView = Utils.findRequiredViewAsType(source, R.id.web_view, "field 'webView'", WebView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    PrivacyActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.webView = null;
  }
}
